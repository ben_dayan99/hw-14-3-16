from bottle import route,run,template,request,post,html_escape
import sqlite3

conn = sqlite3.connect('example.db')
conn.execute('drop table searchers')
conn.execute('''CREATE TABLE IF NOT EXISTS searchers(data text, id integer primary key autoincrement)''')
conn.commit()
@route('/')
def login():
    return '''
        <form action="/" method="post">
            Search: <input name="search" type="text" />
            <input value="click to search" type="submit" />
        </form>
    '''


@post('/') # or @route('/login', method='POST')
def key():
    #search_key = request.forms.get('search')
    a = sqlite3.connect('example.db')
    conn = a.cursor()

    conn.execute("insert into searchers(data) values(?)",(html_escape(request.forms.get('search')),))
    a.commit()

    message_list = "<br>"
    for row in conn.execute("select data from searchers"):
        message_list = message_list + row[0] + "<br>"
    return '''
        <form action="/" method="post">
            Search: <input name="search" type="text" />
            <input value="click to search" type="submit" />
        </form>
    ''' + message_list


run(host='localhost', port=8080)